/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.8125, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)  ", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "125 /logout.php"], "isController": false}, {"data": [1.0, 500, 1500, "Submit Risk without File"], "isController": true}, {"data": [0.0, 500, 1500, "Submit Risk"], "isController": true}, {"data": [1.0, 500, 1500, "Open Login Page"], "isController": true}, {"data": [1.0, 500, 1500, "110 /management/index.php"], "isController": false}, {"data": [1.0, 500, 1500, "123 /api/management/risk/viewhtml"], "isController": false}, {"data": [1.0, 500, 1500, "104 /reports"], "isController": false}, {"data": [0.0, 500, 1500, "Login"], "isController": true}, {"data": [1.0, 500, 1500, "Submit Risk with File Attachment"], "isController": true}, {"data": [1.0, 500, 1500, "58 /index.php"], "isController": false}, {"data": [1.0, 500, 1500, "121 /management/index.php"], "isController": false}, {"data": [1.0, 500, 1500, "Logout"], "isController": true}, {"data": [1.0, 500, 1500, "126 /index.php"], "isController": false}, {"data": [1.0, 500, 1500, "Open Risk Management Menu"], "isController": true}, {"data": [1.0, 500, 1500, "122 /management/index.php"], "isController": false}, {"data": [0.0, 500, 1500, "103 /index.php"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 90, 0, 0.0, 653.0333333333335, 18, 5568, 5383.8, 5499.35, 5568.0, 1.5278579431636845, 10.228077227913964, 2.852813831570638], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "90th pct", "95th pct", "99th pct", "Throughput", "Received", "Sent"], "items": [{"data": ["125 /logout.php", 10, 0, 0.0, 57.0, 49, 82, 80.10000000000001, 82.0, 82.0, 0.18912529550827423, 0.3571956264775414, 0.1566193853427896], "isController": false}, {"data": ["Submit Risk without File", 10, 0, 0.0, 136.60000000000002, 126, 158, 156.9, 158.0, 158.0, 0.1887433467970254, 6.304912517458759, 1.1985202521611114], "isController": true}, {"data": ["Submit Risk", 10, 0, 0.0, 5877.3, 5759, 6056, 6049.3, 6056.0, 6056.0, 0.16960938957580693, 10.218882904857612, 2.8502493523041434], "isController": true}, {"data": ["Open Login Page", 10, 0, 0.0, 35.300000000000004, 19, 169, 154.50000000000006, 169.0, 169.0, 0.1879840589518009, 0.25168568830363186, 0.07016358137829912], "isController": true}, {"data": ["110 /management/index.php", 10, 0, 0.0, 27.299999999999997, 26, 29, 28.9, 29.0, 29.0, 0.1891539145402614, 1.2723925723986609, 0.07979930769667279], "isController": false}, {"data": ["123 /api/management/risk/viewhtml", 10, 0, 0.0, 35.9, 30, 48, 47.9, 48.0, 48.0, 0.18914318138831096, 5.030395900321544, 0.07517702619633063], "isController": false}, {"data": ["104 /reports", 10, 0, 0.0, 27.5, 25, 30, 29.9, 30.0, 30.0, 0.18914675897028504, 0.6471626803987214, 0.15534416434961887], "isController": false}, {"data": ["Login", 10, 0, 0.0, 5495.3, 5411, 5597, 5594.8, 5597.0, 5597.0, 0.17132381915057648, 1.498949570833833, 0.38328685282427316], "isController": true}, {"data": ["Submit Risk with File Attachment", 10, 0, 0.0, 106.30000000000001, 100, 129, 126.9, 129.0, 129.0, 0.1889287738522577, 1.2864684193746458, 1.1694764901757038], "isController": true}, {"data": ["58 /index.php", 10, 0, 0.0, 35.300000000000004, 19, 169, 154.50000000000006, 169.0, 169.0, 0.18820341024579365, 0.2519793705536944, 0.07024545253509994], "isController": false}, {"data": ["121 /management/index.php", 10, 0, 0.0, 106.30000000000001, 100, 129, 126.9, 129.0, 129.0, 0.1889323433278543, 1.2864927249239546, 1.1694985853690794], "isController": false}, {"data": ["Logout", 10, 0, 0.0, 76.69999999999999, 68, 102, 100.30000000000001, 102.0, 102.0, 0.18905736000302492, 0.6088976301659924, 0.23299842609747798], "isController": true}, {"data": ["126 /index.php", 10, 0, 0.0, 19.7, 18, 22, 21.8, 22.0, 22.0, 0.1892469862417441, 0.25208289964232317, 0.07651196514070513], "isController": false}, {"data": ["Open Risk Management Menu", 10, 0, 0.0, 27.299999999999997, 26, 29, 28.9, 29.0, 29.0, 0.1891503366875993, 1.272368505050314, 0.07979779829008095], "isController": true}, {"data": ["122 /management/index.php", 10, 0, 0.0, 100.6, 96, 111, 110.6, 111.0, 111.0, 0.1889144972985227, 1.2863158603166207, 1.1245209246420071], "isController": false}, {"data": ["103 /index.php", 10, 0, 0.0, 5467.699999999999, 5382, 5568, 5566.1, 5568.0, 5568.0, 0.17140604377710358, 0.9132058715140295, 0.24269689733635008], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Percentile 1
            case 8:
            // Percentile 2
            case 9:
            // Percentile 3
            case 10:
            // Throughput
            case 11:
            // Kbytes/s
            case 12:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 90, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
